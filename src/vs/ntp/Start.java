package vs.ntp;

import java.net.InetAddress;
import java.util.Date;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

//List of time servers: http://tf.nist.gov/service/time-servers.html


 
 public class Start {
	 
	// public static final String TIME_SERVER = "time-b.nist.gov";
	 public static final String TIME_SERVER = "utcnist.colorado.edu";
	 


		public static void main(String[] args) throws Exception {
	        NTPUDPClient timeClient = new NTPUDPClient();
	        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
	        System.out.println(inetAddress.toString());
	        TimeInfo timeInfo = timeClient.getTime(inetAddress);
	        long returnTime = timeInfo.getReturnTime();
	        Date time = new Date(returnTime);
	        long systemtime = System.currentTimeMillis();
	        timeInfo.computeDetails();
	        Date realdate = new Date(systemtime + timeInfo.getOffset());
	        System.out.println("Time from " + TIME_SERVER + ": " + time);
	        System.out.println("Time from " + TIME_SERVER + ": " + realdate);
	        System.out.println(""+time.getTime());
	
	
		}	

}
